// pages/videoDetail/videoDetail.js
var getDataservice = require('../../server/project/projectServer.js');
var utils = require('../../utils/util.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    videoList:[],
    videoIndex: null,  //当前那个播放中
    info: {
      menuUrl: "../../images/icon/menu.png",
      testPng: "../../images/icon/test.png",
      shareUrl: "../../images/icon/share.png",
      goodUrl: "../../images/icon/like.png",
      goodedUrl: "../../images/icon/liking.png",
      playURL: "../../images/icon/play.png",
      homeURL: "../../images/icon/home.png"
    },
    data:{}
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    console.log(options)
    getDataservice.toDeatil(options.id).then(res => {
      if(res){
        that.data.videoList.push(res.result);
        that.setData({
          videoList: that.data.videoList
        })
        for (let i = 0; i < that.data.videoList.length; i++) {
          let obj = that.data.videoList[i];
          let is_thumbs_up = 'videoList[' + i + '].is_thumbs_up';
          that.setData({
            [is_thumbs_up]:0
          })
        }
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (options) {
    var that = this;
    that.setData({
      data: options.target.dataset
    });
  
    // 设置菜单中的转发按钮触发转发事件时的转发内容
    var shareObj = {
      title: options.target.dataset.item.forward_title,        // 默认是小程序的名称(可以写slogan等)
      desc: options.target.dataset.item.description,
      path: '/pages/videoDetail/videoDetail?id=' + options.target.dataset.id,        // 默认是当前页面，必须是以‘/’开头的完整路径
      imageUrl: options.target.dataset.item.forward_thumbnail,     //自定义图片路径，可以是本地文件路径、代码包文件路径或者网络图片路径，支持PNG及JPG，不传入 imageUrl 则使用默认截图。显示图片长宽比是 5:4
      success: function (res) {
        // 转发成功之后的回调
        if (res.errMsg == 'shareAppMessage:ok') {
        }
      },
      fail: function (res) {
        // 转发失败之后的回调
        if (res.errMsg == 'shareAppMessage:fail cancel') {
          // 用户取消转发
        } else if (res.errMsg == 'shareAppMessage:fail') {
          // 转发失败，其中 detail message 为详细失败信息
        }
      },
      complete: function () {
        // 转发结束之后的回调（转发成不成功都会执行）
      }
    };
    // 来自页面内的按钮的转发
    if (options.from == 'button') {
      var eData = options.target.dataset;
      console.log(eData.name);     // shareBtn
      // 此处可以修改 shareObj 中的内容
      shareObj.path = '/pages/videoDetail/videoDetail?id=' + options.target.dataset.id;
    　　}
    　　// 返回shareObj
    　　return shareObj;
  },

  videoPlay(event) {
    var length = this.data.videoList.length;
    var index = event.currentTarget.dataset['index'];
    let url = this.data.videoList[index].video_link;
    utils.getRealVideoByUrl(url).then((res) => {
      this.data.videoList[index].video_link = res.url;
      this.setData(
        {
          videoList: this.data.videoList
        });

    }).then(() => {
      //先看看后台有没有需要停止的视频
      if (this.data.stopVideo != null) {
        let stopVideo = wx.createVideoContext('video' + this.data.stopVideo)
        stopVideo.stop()
      }

      if (!this.data.videoIndex) { // 没有播放时播放视频
        this.setData({
          videoIndex: index
        })
        let videoContext = wx.createVideoContext('video' + index)
        console.log(videoContext)
        setTimeout(() => {
          videoContext.play();
        }, 300)
      } else {
        //停止正在播放的视频
        let videoContextPrev = wx.createVideoContext('video' + this.data.videoIndex)
        videoContextPrev.stop()
        //将点击视频进行播放
        this.setData({
          videoIndex: index
        })
        let videoContextCurrent = wx.createVideoContext('video' + index);
        console.log(videoContextCurrent)
        setTimeout(() => {
          videoContextCurrent.play();
        }, 300)
      }

    });


  },
  bindPlay: function () {
    console.log(this.videoContext);
    this.videoContext.play()
  },
  bindPause: function () {
    this.videoContext.pause()
  },
  videoErrorCallback: function (e) {
    console.log('视频错误信息:')
    console.log(e.detail.errMsg)
  },
  routerTo(e) {
    console.log(e.currentTarget.dataset.nickname)
    wx.navigateTo({
      url: '../userList/userList?nickname=' + e.currentTarget.dataset.nickname + '&description=' + e.currentTarget.dataset.description
    })
  },
  //点赞方法
  addGood(event) {
    var that = this;
    var id = event.target.dataset.videoid;
    var index = event.target.dataset.index;
    var obj = that.data.videoList[index];
    var is_thumbs_up = 'videoList[' + index + '].is_thumbs_up'
    var thumbs = 'videoList[' + index + '].thumbs_up'
    if (!that.data.videoList[index].is_thumbs_up) {
      that.setData({
        [is_thumbs_up]: 0
      })
    }
    getDataservice.addGood(obj.is_thumbs_up, id).then(res => {
      if (res) {
        var sum = obj.is_thumbs_up == 0 ? 1 : -1;
        if (obj.is_thumbs_up == 0) {
          that.setData({
            [is_thumbs_up]: 1
          })
        } else {
          that.setData({
            [is_thumbs_up]: 0
          })
        }
        that.setData({
          [thumbs]: parseInt(obj.thumbs_up) + sum
        })
      }
    })
  },

  /**
  * 回到首页(分享的时候)
  */
  backHome: function () {
    this.shopVideo();
    wx.reLaunch({
      url: '/pages/index/index'
    })
  },
  //停止视频
  shopVideo: function () {
    this.setData({
      stopVideo: this.data.videoIndex
    })

    let videoContextPrev = wx.createVideoContext('video' + this.data.stopVideo)
    videoContextPrev.pause()
    setTimeout(() => {
      videoContextPrev.stop();
    }, 300)

    this.setData({
      videoIndex: null
    })
  },
})