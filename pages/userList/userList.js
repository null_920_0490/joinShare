//index.js
//获取应用实例
const app = getApp();
var getDataservice = require('../../server/project/projectServer.js');
var utils = require('../../utils/util.js');
var events = require('../../utils/event.js');
Page({
  onReady: function (res) {
    this.videoContext = wx.createVideoContext('myVideo');
    //获得popup组件
  },
  data: {
    scrollTop: 0,
    scrollHeight: 0,
    page: 0,
    pageSize: 30,
    category: 0,
    videoList: [],
    playStaus: false,
    videoIndex: null,  //当前那个播放中
    loading: true, //加载的时候
    info: {
      menuUrl: "../../images/icon/菜单.png",
      testPng: "../../images/icon/test.png",
      shareUrl: "../../images/icon/分享.png",
      goodUrl: "../../images/icon/like.png",
      goodedUrl: "../../images/icon/liking.png",
      playURL: "../../images/icon/play.png"
    },
    hasMoreData: false,
    isRefreshing: false,
    nickname:"",
    description:"",
    stopVideo: null, //需要停止的视频
  },
  onLoad: function (options) {
    var that = this;
    wx.getSystemInfo({
      success: function (res) {
        console.info(res.windowHeight);
        that.setData({
          scrollHeight: res.windowHeight,
          nickname: options.nickname,
          description: options.description
        });
      }
    });
    that.getUserVideo(this.data.page, this.data.pageSize, this.data.nickname, false);
  },
  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    this.shopVideo()
  },

  getUserVideo: function (pageNo, pageSize, nickname, more) {
    var that = this;
    getDataservice.getUserVideo(pageNo, pageSize, nickname).then(res => {
      this.setData({
        loading: false,
      })
      that.initRefresh();
      if (more) {
        if (res.result) {
          var list = that.data.videoList.concat(res.result);
          that.setData({
            videoList: list
          });
          for (let i = 0; i < that.data.videoList.length; i++) {
            let is_thumbs_up = 'videoList[' + i + '].is_thumbs_up';
            that.setData({
              [is_thumbs_up]: 0
            })
          }
          setTimeout(function () {
            that.initMore();
          }, 300);
        } else {
          that.initMore();
          that.setData({
            lastBottom: true
          });
        }
      } else {
        if (res.result) {
          var list = that.data.videoList.concat(res.result);
          setTimeout(function () {
            that.setData({
              videoList: res.result
            });
            that.setLike()
          }, 300);
         
        }
      }
      
     
    })
  },

  //添加默认已经点击了赞
  setLike: function () {
    var hasLike;
    var that = this;
    for (let i = 0; i < that.data.videoList.length; i++) {
      let is_thumbs_up = 'videoList[' + i + '].is_thumbs_up';
      that.setData({
        [is_thumbs_up]: 0
      })
    }
    if (wx.getStorageSync("likeFlag") == "") {
      hasLike = [];
    } else {
      hasLike = wx.getStorageSync("likeFlag");
      hasLike.forEach((data) => {
        this.data.videoList.forEach((item, i) => {
          console.log(data, item.id)
          if (data == item.id) {
            let is_thumbs_up = 'videoList[' + i + '].is_thumbs_up';
            that.setData({
              [is_thumbs_up]: 1
            })
          }
        })
      })
    }
  },

  initRefresh() {
    this.setData({
      isRefreshing: false,
    })
  },

  initMore() {
    this.setData({
      hasMoreData: false
    })
  },

//下拉刷新方法
  onPullDownRefresh: function () {
    this.shopVideo();
    
    if (this.data.isRefreshing) {
      return
    } else {
      wx.stopPullDownRefresh();
      this.setData({
        isRefreshing: true,
        videoList:[]
      })
      
      this.getUserVideo(0, 30, this.data.nickname,false);
    }
  },
  onReachBottom: function () {
    if (this.data.lastBottom) {
      return;
    } else {
      this.setData({
        hasMoreData: true,
        page: parseInt(this.data.page) + 1
      });
      this.getUserVideo(this.data.page, this.data.pageSize, this.data.category, true)
    }
  },
  
  //点赞方法
  addGood(event) {
    var that = this;
    var id = event.target.dataset.videoid;
    var index = event.target.dataset.index;
    var obj = that.data.videoList[index];
    var is_thumbs_up = 'videoList[' + index + '].is_thumbs_up'
    var thumbs = 'videoList[' + index + '].thumbs_up'
    getDataservice.addGood(obj.is_thumbs_up,id).then(res => {
      if (res) {
        var sum = obj.is_thumbs_up == 0 ? 1 : -1;
        if (obj.is_thumbs_up == 0) {
          that.setData({
            [is_thumbs_up]: 1
          })
          getDataservice.likeFlag(1, id)
          events.emit("isLike", {
            type: 1,
            id: id,
            number: parseInt(obj.thumbs_up) + sum
          })
        } else {
          that.setData({
            [is_thumbs_up]: 0
          })
          getDataservice.likeFlag(0, id)
          events.emit("isLike", {
            type: 0,
            name: id,
            number: parseInt(obj.thumbs_up) + sum
          })
        }
        that.setData({
          [thumbs]: parseInt(obj.thumbs_up) + sum
        })
      }
    })
    console.log(event)
   
  },

  videoPlay(event) {
    var length = this.data.videoList.length;
    var index = event.currentTarget.dataset['index'];
    let url = this.data.videoList[index].video_link;
    utils.getRealVideoByUrl(url).then((res) => {
      this.data.videoList[index].video_link = res.url;
      this.setData(
        {
          videoList: this.data.videoList
        });

    }).then(() => {
      //先看看后台有没有需要停止的视频
      if (this.data.stopVideo != null) {
        let stopVideo = wx.createVideoContext('video' + this.data.stopVideo)
        stopVideo.stop()
      }

      if (!this.data.videoIndex) { // 没有播放时播放视频
        this.setData({
          videoIndex: index
        })
        let videoContext = wx.createVideoContext('video' + index)
        console.log(videoContext)
        setTimeout(()=> {
          videoContext.play();
        }, 300)
      } else {
        //停止正在播放的视频
        let videoContextPrev = wx.createVideoContext('video' + this.data.videoIndex)
        videoContextPrev.stop()
        //将点击视频进行播放
        this.setData({
          videoIndex: index
        })
        let videoContextCurrent = wx.createVideoContext('video' + index);
        console.log(videoContextCurrent)
        setTimeout(() => {
          videoContextCurrent.play();
        }, 300)
      }

    });

    
  },
  //停止视频
  shopVideo:function(){
    this.setData({
      stopVideo: this.data.videoIndex
    })

    let videoContextPrev = wx.createVideoContext('video' + this.data.stopVideo)
    videoContextPrev.pause()
    setTimeout(() => {
      videoContextPrev.stop();
    }, 300)

    this.setData({
      videoIndex: null
    })
  },


  bindPlay: function () {
    console.log(this.videoContext);
    this.videoContext.play()
  },
  bindPause: function () {
    this.videoContext.pause()
  },
  videoErrorCallback: function (e) {
    console.log('视频错误信息:')
    this.shopVideo();
    console.log(e.detail.errMsg)
  },
  videoEnd: function (res) {
    this.shopVideo();
    console.log(res.target.dataset)
    console.log(this.data.videoIndex)
    this.setData({
      videoIndex: res.target.dataset.index + 1
    })
    let url = this.data.videoList[this.data.videoIndex].video_link;
    console.log(this.data.videoIndex)
    utils.getRealVideoByUrl(url).then((res) => {
      this.data.videoList[this.data.videoIndex].video_link = res.url;
      this.setData(
        {
          videoList: this.data.videoList
        });

    }).then(() => {

      var videoContextCurrent = wx.createVideoContext('video' + this.data.videoIndex);
      setTimeout(function () {
        videoContextCurrent.play();
      },300)
     

    });

  },
  onShareAppMessage: function (options) {
    var that = this;
    that.setData({
      data: options.target.dataset
    });

    // 设置菜单中的转发按钮触发转发事件时的转发内容
    var shareObj = {
      title: options.target.dataset.item.forward_title,        // 默认是小程序的名称(可以写slogan等)
      desc: options.target.dataset.item.description,
      path: '/pages/videoDetail/videoDetail?id=' + options.target.dataset.id,        // 默认是当前页面，必须是以‘/’开头的完整路径
      imageUrl: options.target.dataset.item.forward_thumbnail,     //自定义图片路径，可以是本地文件路径、代码包文件路径或者网络图片路径，支持PNG及JPG，不传入 imageUrl 则使用默认截图。显示图片长宽比是 5:4
      success: function (res) {
        // 转发成功之后的回调
        if (res.errMsg == 'shareAppMessage:ok') {
        }
      },
      fail: function (res) {
        // 转发失败之后的回调
        if (res.errMsg == 'shareAppMessage:fail cancel') {
          // 用户取消转发
        } else if (res.errMsg == 'shareAppMessage:fail') {
          // 转发失败，其中 detail message 为详细失败信息
        }
      },
      complete: function () {
        // 转发结束之后的回调（转发成不成功都会执行）
      }
    };
    // 来自页面内的按钮的转发
    if (options.from == 'button') {
      var eData = options.target.dataset;
      console.log(eData.name);     // shareBtn
      // 此处可以修改 shareObj 中的内容
      shareObj.path = '/pages/videoDetail/videoDetail?id=' + options.target.dataset.id;
    }
    // 返回shareObj
    return shareObj;
  },

})