//index.js
//获取应用实例
const app = getApp();
var getDataservice = require('../../server/project/projectServer.js');
var utils = require('../../utils/util.js');
var events = require('../../utils/event.js');
Page({
  onReady: function (res) {
    this.videoContext = wx.createVideoContext('myVideo');
      //获得popup组件
    this.popup = this.selectComponent("#popup");
  },
  data: {
    scrollTop: 0,
    scrollHeight: 0,
    page: 0,
    adsPage: 0, //广告页
    pageSize:5,
    category:0,
    videoList:[],
    adsList:[],
    navMenu: false,
    videoIndex: null,  //当前那个播放中
    video:null,
    playStaus:false,
    lastBottom: false,
    scrollerShow: false,
    changeShow: false,
    isChange: "",  //换一批状态,
    addOne:"",  //点赞动画
    stopVideo:null, //需要停止的视频
    canGetData:true,  //
    noData: false, //没有数据的时候
    loading: true, //加载的时候
    navArr:[
      { id: 1001, name: "推荐" },
      { id: 0, name: "搞笑" },
      { id: 1, name: "旅行" },
      { id: 2, name: "萌娃萌宠" },
      { id: 3, name: "美妆" },
      { id: 4, name: "科技" },
      { id: 5, name: "运动" },
      { id: 6, name: "音乐" },
      { id: 7, name: "时尚" }
    ],
    section:[],
    currentId: 1001,
    info:{
      menuUrl:"../../images/icon/menu.png",
      testPng: "../../images/icon/test.png",
      shareUrl: "../../images/icon/share.png",
      goodUrl: "../../images/icon/like.png",
      goodedUrl: "../../images/icon/liking.png",
      playURL: "../../images/icon/play.png"
    },
    hasMoreData: false,
    isRefreshing: false,
    adsAvatarUrl:"https://yz.62fans.cn/uploads/"
  },
  onLoad: function () {
    var that = this;
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          scrollHeight: res.windowHeight
        });
      }
    });
    if (that.data.navArr.length > 5 ) {
      that.setData({
        section: that.data.navArr.splice(0,5),
        navMenu: true
      })
    }else {
      that.setData({
        section: that.data.navArr
      })
    }
    that.getVideo(that.data.page, that.data.pageSize, that.data.currentId,false);

    events.on("isLike", this , function(data){
        console.log(data)
      this.data.videoList.forEach((item, i) => {
        console.log(item)
        if (data.type == 1){
          if(item.id == data.id){
            var thumbs = 'videoList[' + i + '].thumbs_up';
          
            that.setData({
              [thumbs]: data.number
            })
          }
        }else{
          var thumbs = 'videoList[' + i + '].thumbs_up';

          that.setData({
            [thumbs]: data.number
          })
        }
      })
    })
  },

  //获取广告方法
  getAd(pageNo, pageSize, category,boo, length) {
    var that = this;
    let is_recommend;
    let sendCategory;
    if (category == 1001) {
      is_recommend = 1;
      sendCategory = false;
    } else {
      is_recommend = 0;
      sendCategory = category;
    }
    getDataservice.getAds(pageNo, pageSize, is_recommend, sendCategory).then(res => {
      if (res.result && that.data.videoList.length > 5*(that.data.page+1)-1){
        var list = this.data.videoList.concat(res.result);
        that.setData({
          videoList: list,
          adsPage: that.data.adsPage + 1,
          canGetData: true
        });
      } else {
        console.log(length)
        //如果没有广告的时候，就传个空的过去，HTML去判断这个字段是否为空
        if (length == 5){
          // that.setData({
          //   adsPage: 0,
          // })
          // getDataservice.getAds(that.data.adsPage, 1, is_recommend, sendCategory).then(val => {
          //   if (val.result && that.data.videoList.length > 5 * (that.data.page + 1) - 1) {
          //     var list = this.data.videoList.concat(val.result);
          //     that.setData({
          //       videoList: list,
          //       adsPage: that.data.adsPage + 1,
          //       canGetData: true
          //     });
          //   }
          // })


          let ads = {
            ads_pic: ""
          }
          let list = this.data.videoList.concat(ads);
          that.setData({
            videoList: list,
            canGetData: true
          })
        }else{
          console.log("aaaaa")
        }
        
        that.setData({
          adsPage: 0,
          canGetData:true
        })
      }
    })
  },


  //点赞方法
  addGood(event) {
    var that = this;
    var id = event.target.dataset.videoid;
    var index = event.target.dataset.index;
    var obj = that.data.videoList[index];
    var is_thumbs_up = 'videoList[' + index + '].is_thumbs_up'
    var thumbs = 'videoList[' + index + '].thumbs_up'
    getDataservice.addGood(obj.is_thumbs_up, id).then(res => {
      if (res) {
        var sum = obj.is_thumbs_up == 0 ? 1 : -1;
        if (obj.is_thumbs_up == 0) {
          that.setData({
            [is_thumbs_up]: 1
          })
          getDataservice.likeFlag(1 , id)
        } else {
          that.setData({
            [is_thumbs_up]: 0
          })
          getDataservice.likeFlag(0, id)
        }
        that.setData({
          [thumbs]: parseInt(obj.thumbs_up) + sum
        })
      }
    })
  },
  
  //获取视频列表数据方法
  getVideo: function (pageNo, pageSize, category,more,type) {
  
    this.setData({
      changeShow: false,
      lastBottom: false,
    })
    wx.stopPullDownRefresh();
    var that = this;
    let is_recommend;
    let sendCategory;
    if (category == 1001) {
      is_recommend = 1;
      sendCategory = false;
    } else {
      is_recommend = 0;
      sendCategory = category;
    }
    getDataservice.getVideoList(pageNo, pageSize, is_recommend, sendCategory).then(res => {
      this.setData({
        loading: false,
      })
      let thisResLength;
      if (res.result){
        thisResLength = res.result.length;
      }else{
        thisResLength = 0;
      }
    
      if(more) {
        
        console.log(res.result)
        if (res.result) {
          //setTimeout(function () {
            let array = [];
            if (that.data.isChange == 'change') {
              array = res.result.sort(that.randomsort)
            }else{
              array = res.result
            }
            console.log(array)
            var list = that.data.videoList.concat(array);
            
            
            that.setData({
              videoList: list,
              canGetData: false,
              noData: false
            });
            that.setLike();
            
            that.initMore();
          if (res.result.length<5){
            that.setData({
              canGetData: true
            });
          }else{
            that.getAd(that.data.adsPage, 1, that.data.currentId, false, thisResLength);
          }
      
           
         // }, 0);
        }else {
          that.initMore();
          that.setData({
            lastBottom: true
          });
        }
      } else {
        console.log(res.result)
        if (res.result != undefined) {
          if (that.data.isChange != 'change') {
            that.setData({
              videoList: res.result,
              scrollerShow: true,
              lastBottom: false,
              page: 0,
              canGetData: false,
              noData: false
            });       
            setTimeout(function(){
              that.initRefresh();
            },1000)
          } else {
            that.setData({
              videoList: res.result.sort(that.randomsort),
              scrollerShow: true,
              canGetData: false,
              noData: false
            });
            that.initRefresh();
          }
      
          that.getAd(that.data.adsPage, 1, that.data.currentId, false, thisResLength);
        } else {
          that.setData({
            videoList: [],
            noData: true
          })
        }
      }
      that.setLike();
      
      if (res.msg == "no data" && that.data.videoList.length > 1) {
        that.setData({
          changeShow: true,
        })
      }
      console.log(that.data.videoList);
    })
  },


  //添加默认已经点击了赞
  setLike:function(){
    var hasLike;
    var that = this;
    for (let i = 0; i < that.data.videoList.length; i++) {
      let is_thumbs_up = 'videoList[' + i + '].is_thumbs_up';
      that.setData({
        [is_thumbs_up]: 0
      })
    }
    if (wx.getStorageSync("likeFlag") == "") {
      hasLike = [];
    } else {
      hasLike = wx.getStorageSync("likeFlag");
      hasLike.forEach((data) => {
        this.data.videoList.forEach((item, i) => {
          if (data == item.id) {
            let is_thumbs_up = 'videoList[' + i + '].is_thumbs_up';
            that.setData({
              [is_thumbs_up]: 1
            })
          }
        })
      })
    } 
  },
  /**
  * 生命周期函数--监听页面显示
  */
  onShow: function () {
    this.setLike()
  },

  //打乱数组方法
  randomsort(a, b) {
    return Math.random() > .5 ? -1 : 1;
    //用Math.random()函数生成0~1之间的随机数与0.5比较，返回-1或1
  },
  
  initRefresh(){
    this.setData({
      isRefreshing: false,
    })
  },

  routerTo(e) {
    console.log(e.currentTarget.dataset.nickname)
    this.shopVideo()
    wx.navigateTo({
      url: '../userList/userList?nickname=' + e.currentTarget.dataset.nickname + '&description=' + e.currentTarget.dataset.description
    })
  },

  initMore() {
    this.setData({
      hasMoreData: false
    })
  },

  //更多栏目按钮方法
  showPopup() {
    this.setData({
      videoIndex: null,
      changeShow: false
    })
    this.popup.showPopup();
  },

  //取消事件
  _close() {
    this.setData({
      changeShow: true,
      canGetData:true,
    })
  },
  //确认事件
  _success(obj) {
    var that = this;
    var id = obj.detail;
    that.setData({
      currentId : id,
      videoIndex: null,
      adsPage: 0,
      isChange:""
    });
    that.popup.hidePopup();
    this.goTop()
    that.getVideo(0, 5, that.data.currentId,false); 
  },

  //下拉刷新方法
  onPullDownRefresh: function () {
    var that = this;
    this.shopVideo();
    wx.stopPullDownRefresh();
    if (that.data.isRefreshing) {
      return
    } else {
      that.setData({
        isRefreshing: true
      })
      that.getVideo(0, 5, that.data.currentId, false)
    }
  },

  //上拉加载更多
  onReachBottom: function () {
    var that = this;
    if (that.data.lastBottom || that.data.hasMoreData) {
      return;
    }else {
      if (that.data.canGetData){
        that.setData({
          hasMoreData: true,
          page: parseInt(that.data.page) + 1
        });
        that.getVideo(that.data.page, that.data.pageSize, that.data.currentId, true);
      }
    }
  },

  videoPlay(event) {
    
    var length = this.data.videoList.length;
    var index = event.currentTarget.dataset['index'];

    let url = this.data.videoList[index].video_link;
    console.log(url)
    utils.getRealVideoByUrl(url).then((res) => {
      this.data.videoList[index].video_link = res.url;
      this.setData(
        {
          videoList: this.data.videoList
        });

    }).then(()=>{
      //先看看后台有没有需要停止的视频
      if (this.data.stopVideo != null){
        let stopVideo = wx.createVideoContext('video' + this.data.stopVideo)
        stopVideo.stop()
      }

      if (!this.data.videoIndex) { // 没有播放时播放视频
        this.setData({
          videoIndex: index
        })
        var videoContext = wx.createVideoContext('video' + index)
        console.log(videoContext)
        setTimeout(() => {
          videoContext.play();
        }, 300)
        
      } else {
        //停止正在播放的视频
        var videoContextPrev = wx.createVideoContext('video' + this.data.videoIndex)
        videoContextPrev.stop()
        //将点击视频进行播放
        this.setData({
          videoIndex: index
        })
        var videoContextCurrent = wx.createVideoContext('video' + index)
        setTimeout(() => {
          videoContextCurrent.play();
        }, 300)
      
      }

    });
  },
  //停止视频
  shopVideo: function () {
    this.setData({
      stopVideo: this.data.videoIndex
    })
    
    let videoContextPrev = wx.createVideoContext('video' + this.data.stopVideo)
    videoContextPrev.pause()
    setTimeout(() => {
      videoContextPrev.stop();
    },300)
  
    this.setData({
      videoIndex: null
    })
  },
  bindPlay: function () {
    console.log(this.videoContext);
    this.videoContext.play()
  },
  bindPause: function () {
    this.videoContext.pause()
  },
  videoErrorCallback: function (e) {
    console.log('视频错误信息:')
    this.shopVideo()
    console.log(e.detail.errMsg)
  },

  //换一批方法
  changeList(event) {
    this.setData({
      videoList: [],
      lastBottom: false,
      page:0,
      isChange:"change",
      canGetData: true
    })
    this.initMore();
    this.initRefresh();
    this.getVideo(0, 5, this.data.currentId, false, this.data.isChange)
  },

  selectMenuItem: function(e){
    var that = this;
    this.shopVideo();
    that.setData({
      currentId: e.currentTarget.dataset.detail.id,
      videoIndex: null,
      lastBottom: false,
      page: 0,
      adsPage: 0,
      isChange:"",
      canGetData:true
    });
    this.goTop();
    that.getVideo(that.data.page, that.data.pageSize, that.data.currentId);
  },
  videoEnd: function (res) {   
    this.shopVideo();
    this.setData({
      videoIndex: res.target.dataset.index + 1
    })
    let url = this.data.videoList[this.data.videoIndex].video_link;
    
    utils.getRealVideoByUrl(url).then((res) => {
      this.data.videoList[this.data.videoIndex].video_link = res.url;
      this.setData(
        {
          videoList: this.data.videoList
        });

    }).then(() => {

      var videoContextCurrent = wx.createVideoContext('video' + this.data.videoIndex);
      videoContextCurrent.play();

    });
    
  },
  toNews: function(e){   
    console.log(e)
    var obj = e.target.dataset.obj;
    if (obj.link_type == "0") {
      wx.navigateTo({
        url: '../webview/webview?url=' + obj.ads_link.replace(/<[^>]+>/g, "")
      })
    } else if (obj.link_type == "1"){
      let data = JSON.stringify(obj)
      wx.setStorage({
        key: "adsDetails",
        data: data
      })
      wx.navigateTo({
        url: '../adsDetails/adsDetails'
      })
    }
  },
  toDetail(e){
    
    wx.navigateTo({
      url: '../videoDetail/videoDetail?videoId=' + e.currentTarget.dataset.item.id
    })
  },
  onShareAppMessage: function (options) {
    console.log(options.target.dataset.item.forward_thumbnail)
    var that = this;

    // 设置菜单中的转发按钮触发转发事件时的转发内容
    var shareObj = {
      title: options.target.dataset.item.forward_title,        // 默认是小程序的名称(可以写slogan等)
      desc: options.target.dataset.item.description,
      path: '/pages/videoDetail/videoDetail?id=' + options.target.dataset.id,        // 默认是当前页面，必须是以‘/’开头的完整路径
      imageUrl: options.target.dataset.item.forward_thumbnail,     //自定义图片路径，可以是本地文件路径、代码包文件路径或者网络图片路径，支持PNG及JPG，不传入 imageUrl 则使用默认截图。显示图片长宽比是 5:4
      success: function (res) {
        // 转发成功之后的回调
        if (res.errMsg == 'shareAppMessage:ok') {
        }
      },
      fail: function (res) {
        // 转发失败之后的回调
        if (res.errMsg == 'shareAppMessage:fail cancel') {
          // 用户取消转发
        } else if (res.errMsg == 'shareAppMessage:fail') {
          // 转发失败，其中 detail message 为详细失败信息
        }
      },
      complete: function () {
        // 转发结束之后的回调（转发成不成功都会执行）
      }
    };
    // 来自页面内的按钮的转发
    if (options.from == 'button') {
      // 此处可以修改 shareObj 中的内容
      shareObj.path = '/pages/videoDetail/videoDetail?id=' + options.target.dataset.id;
    }
    // 返回shareObj
    return shareObj;
  },
  //回到顶部
  goTop: function () {  // 一键回到顶部
    if (wx.pageScrollTo) {
      wx.pageScrollTo({
        scrollTop: 0
      })
    } else {
      wx.showModal({
        title: '提示',
        content: '当前微信版本过低，无法使用该功能，请升级到最新微信版本后重试。'
      })
    }
  }
})