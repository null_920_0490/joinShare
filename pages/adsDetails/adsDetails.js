var WxParse = require('../../plugins/wxParse/wxParse.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    operator:'',
    theme:'',
    updateTime:'',
    pageview: '',
    content:""
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let thisStorage = JSON.parse(wx.getStorageSync('adsDetails'));
    
    //console.log(thisStorage)
    let article = thisStorage.ads_link;
    
    console.log(article)
    this.setData({
      operator: thisStorage.operator,
      theme: thisStorage.title,
      updateTime: thisStorage.updated_at,
      pageview:thisStorage.pageview,
      content: article
    })
    wx.removeStorageSync('adsDetails');
    WxParse.wxParse('article', 'html', article, this, 5);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  }
})