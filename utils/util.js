const formatTime = date => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()

  return [year, month, day].map(formatNumber).join('/') + ' ' + [hour, minute, second].map(formatNumber).join(':')
}

const formatNumber = n => {
  n = n.toString()
  return n[1] ? n : '0' + n
}

const getVid = url => {
  var chars = url.split("/");
  var vid = chars[chars.length - 1].split('.')[0];
  return vid;
}

const getRealVideoByUrl = url => {

  var vid = getVid(url);
  if(vid == undefined){
    alert(请输入有效视频地址);
    return;
  }
  return new Promise(function (resolve, reject) {
    wx.request({
      url: "https://vv.video.qq.com/getinfo?vids=" + vid + "&platform=101001&charge=0&otype=json",
      dataType: "json",
      method: 'GET',
      success: function (resp) {
        var data = resp.data.split('=')[1].slice(0, -1)
        var res = JSON.parse(data);
        var url = res.vl.vi[0].ul.ui[0].url;
        var fn = res.vl.vi[0].fn;
        var fvkey = res.vl.vi[0].fvkey;
        var result = url + fn + '?vkey=' + fvkey;
        var vurl = {
          url: result
        }
        resolve(vurl);

      },
      fail: function (data) {
        reject(data);
      }
    });
  })
};


module.exports = {
  formatTime: formatTime,
  getRealVideoByUrl: getRealVideoByUrl
}
