var rootURL = require('../../utils/constants.js').DOMAIN;
  
function getVideoList(pageNo, pageSize, is_recommend, category) {
  let data = {};
  if (is_recommend == 0) {
    data = {
      page: pageNo,
      page_size: pageSize,
      category: parseInt(category)
    }
  } else {
    data = {
      page: pageNo,
      page_size: pageSize,
      is_recommend: 1,
    }
  }
  
  return new Promise((resolve,reject)=>{
    wx.request({
      url: rootURL + '/api/videos',
      data: data,
      method: 'GET',
      success: res => {
        resolve(res.data);
      },
      fail: data => {
        reject(data);
      }
    });
  })
};

function getUserVideo(page, pageSize, nickname) {
  return new Promise((resolve, reject) => {
    wx.request({
      url: rootURL + '/api/user_video_list',
      data: {
        page: page,
        page_size: pageSize,
        nickname: nickname
      },
      method: 'GET',
      success: res => {
        resolve(res.data);
      },
      fail: data => {
        reject(data);
      }
    });
  })
};

function addGood(good,id) {
  return new Promise((resolve,reject)=>{
    wx.request({
      url: rootURL + '/api/incr_thumbs_up',
      data: {
        is_thumbs_up :good,
        video_id: id
      },
      method: 'GET',
      success: res => {
        resolve(res.data);
      },
      fail: data => {
        reject(data);
      }
    });
  })
};

function getAds(pageNo, pageSize, is_recommend, category) {
  let data = {};
  if (is_recommend == 0) {
    data = {
      page: pageNo,
      page_size: pageSize,
      category: parseInt(category)
    }
  } else {
    data = {
      page: pageNo,
      page_size: pageSize,
      is_recommend: 1,
    }
  }
  return new Promise((resolve,reject)=>{
    wx.request({
      url: rootURL + '/api/ads',
      data: data,
      method: 'GET',
      success: res => {
        resolve(res.data);
      },
      fail: data => {
        reject(data);
      }
    });
  })
};

function toDeatil(id) {
  let data = {};
  return new Promise((resolve, reject) => {
    wx.request({
      url: rootURL + '/api/show/'+id,
      data: data,
      method: 'GET',
      success: res => {
        resolve(res.data);
      },
      fail: data => {
        reject(data);
      }
    });
  })
};

//点赞方法  0是取消 1是添加
function likeFlag(type , id) {
  console.log(id)
  var hasLike;
  if (wx.getStorageSync("likeFlag") == ""){
    hasLike = [];
  }else{
    hasLike = wx.getStorageSync("likeFlag");
  }
  if(type == 1){
    hasLike.push(id);
    wx.setStorage({
      key: "likeFlag",
      data: unique1(hasLike)
    })
  }else{
    let index = hasLike.indexOf(id);
    hasLike.splice(index, 1);
    wx.setStorage({
      key: "likeFlag",
      data: unique1(hasLike)
    })
  }

  console.log(wx.getStorageSync("likeFlag"))
}

//数组去重
function unique1(arr) {
  var hash = [];
  for (var i = 0; i < arr.length; i++) {
    if (hash.indexOf(arr[i]) == -1) {
      hash.push(arr[i]);
    }
  }
  return hash;
}


module.exports = {
  getVideoList: getVideoList,
  getUserVideo: getUserVideo,
  addGood: addGood,
  getAds: getAds,
  toDeatil: toDeatil,
  likeFlag: likeFlag
} 